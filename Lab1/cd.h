#pragma once

#include <unistd.h>
#include <experimental/filesystem>
#include <iterator>
#include <regex>
#include <iostream>

namespace fs = std::experimental::filesystem;

void cd(std::string path) {
    std::string currentPath = getenv("PWD");
    if(path == "..") {
        unsigned long temp = currentPath.find_last_not_of("/");
        if(temp == currentPath.npos) {
            currentPath = "/";
        } else {
            currentPath = currentPath.substr(0, temp);
            currentPath = currentPath.substr(0, currentPath.find_last_of("/"));
        }
        if(currentPath == "") currentPath = "/";
    }
    else if(path == ".") return;
    else if(path[0] == '~') {
        currentPath = getenv("HOME");
        if(path.substr(1).size() > 0) currentPath += path.substr(1);
        if(!fs::exists(currentPath)) currentPath = getenv("PWD");
    }
    else if(path[0] == '/') {
        if(fs::exists(path)) currentPath = path;
    }
    else if(fs::exists(currentPath + "/" + path)) currentPath = currentPath + "/" + path;
    else if(fs::exists(path)) currentPath = path;
    else return;

    std::regex reg("[/]{2,}");
    currentPath = std::regex_replace(currentPath, reg, "/");
    if(setenv("PWD", currentPath.c_str(), 1)) {
        perror("PWD Error");
    }
    chdir(currentPath.c_str());
    //currentPath = getenv("PWD");
    //std::cout << currentPath << std::endl;
}
