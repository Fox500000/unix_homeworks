#pragma once

#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


using namespace std;

void mkdir(string name)
{
    string currentPath = getenv("PWD");
    string fullPath = currentPath + "/" + name;
    if (mkdir(fullPath.c_str(),S_IRWXU | S_IRWXG | S_IRWXO) == 0)
        return;
    else
        perror("mkdir error");
}
