#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "ls.h"
#include "ps.h"
#include "cd.h"
#include "free.h"
#include "rmdir.h"
#include "mkdir.h"
#include "varparser.h"
#include "printstd.h"
#include "tail.h"

int main() {
    chdir(getenv("PWD"));
    std::string buffer;

    while(true) {
        std::string path = getenv("PWD");
        print(path + ":");
        std::vector<std::string> commands;
        std::string input;
        //getline(std::cin, input);
        input = readLine();
        input = varParse(input);
        unsigned long pos;

        while(input.size() > 0) {
            if((pos = input.find("|")) != input.npos) {
                std::string temp = input.substr(0, pos);
                commands.push_back(temp);
                input = input.substr(pos + 1);
            } else {
                unsigned long start = input.find_first_not_of(" ");
                unsigned long end = input.find_last_not_of(" ");
                input = input.substr(start, end + 1 - start);
                //cout << input << endl;
                commands.push_back(input);
                input = "";
            }
        }

        int defaultPipe[2];
        int prevPipe[2];
        int nextPipe[2];
        bool pipeItIs = false;
        if(commands.size() > 1) {
            //Saving default IO
            defaultPipe[0] = dup(STDIN_FILENO);
            defaultPipe[1] = dup(STDOUT_FILENO);
            prevPipe[0] = STDIN_FILENO;
            prevPipe[1] = STDOUT_FILENO;
            pipe(nextPipe);
            pipeItIs = true;
        }
        //0 - IN 1 - OUT
        for(unsigned long i = 0; i < commands.size(); i++) {
            // All output to STDOUT now comes to nextPipe[1]
            if(pipeItIs) dup2(nextPipe[1], STDOUT_FILENO);

            std::stringstream ss(commands[i]);
            std::string t;
            getline(ss, t, ' ');
            if(t == "cd") {
                getline(ss, t, ' ');
                cd(t);
                continue;
            }
            else if(t == "ls") {
                bool l = false;
                bool i = false;
                bool a = false;
                std::string path = "";
                while(getline(ss, t, ' ')) {
                    if(t == "-l") l = true;
                    else if(t == "-i") i = true;
                    else if(t == "-a") a = true;
                    else path = t;
                }
                if(path != "") {
                    ls(path, l, i, a);
                } else {
                    ls(".", l, i, a);
                }
            }
            else if(t == "ps") ps();
            else if(t == "free") free();
            else if(t == "rmdir") {
                getline(ss, t, ' ');
                rmdir(t);
            }
            else if(t == "mkdir") {
                getline(ss, t, ' ');
                mkdir(t);
            }
            else if(t == "tail") {
                int n = 5;
                std::string in = "";
                while(getline(ss, t, ' ')) {
                    if(t == "-n") {
                        getline(ss, t, ' ');
                        n = stoi(t);
                        continue;
                    }
                    in = t;
                }
                tail(in, n);
            }
            else if(t == "exit") return 0;
            else {
                std::string temp = t;
                while(getline(ss, t, '\n')) {
                    temp += " " + t;
                }
                system(temp.c_str());
            }
            if(pipeItIs) {
                close(prevPipe[0]);
                //All output to STDIN now comes to nextPipe[0]
                dup2(nextPipe[0], STDIN_FILENO);
                if(i < commands.size() - 1) close(nextPipe[1]);

                prevPipe[0] = nextPipe[0];
                prevPipe[1] = nextPipe[1];
                if(i == commands.size() - 2) {
                    //Reverting to default IO
                    nextPipe[0] = defaultPipe[0];
                    nextPipe[1] = defaultPipe[1];
                } else {
                    pipe(nextPipe);
                }
            }
        }
        // Should be used when errors occur
        if(pipeItIs && errno) {
            close(nextPipe[0]);
            dup2(defaultPipe[0], STDIN_FILENO);
            dup2(defaultPipe[1], STDOUT_FILENO);
            close(defaultPipe[0]);
            close(defaultPipe[1]);
        }
    }
    return 0;
}
