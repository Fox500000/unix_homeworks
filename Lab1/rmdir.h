#pragma once

#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>


using namespace std;

void rmdir(string name)
{
    string currentPath = getenv("PWD");
    string fullPath = currentPath + "/" + name;
    if (rmdir(fullPath.c_str()) == 0)
        return;
    else
        perror("rmdir error");
}
