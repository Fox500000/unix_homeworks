#pragma once

#include <string>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <iostream>
#include <iomanip>
#include <pwd.h>
#include <grp.h>

#include "printstd.h"

void ps() {
    DIR *directory;
    struct dirent *ent;
    if((directory = opendir("/proc")) != NULL) {
        while((ent = readdir(directory)) != NULL) {
            struct stat st;
            std::string path = "/proc/";
            path += ent->d_name;
            path += "/comm";
            std::ifstream f;
            f.open(path);
            std::string name;
            std::getline(f, name);
            f.close();
            if(stat(path.c_str(), &st) == 0) {
                print(name + "\t" + ent->d_name + "\t" + getpwuid(st.st_uid)->pw_name + "\t" + getgrgid(st.st_gid)->gr_name + '\n');
            }
        }
    } else {
        perror("PS error");
    }
}
