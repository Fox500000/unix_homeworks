#pragma once

#include "printstd.h"
#include <fstream>
#include <sstream>
#include <vector>

void tail(std::string file, int n) {
    std::vector<std::string> lastLines;
    if(file != "") {
        std::string f = getenv("PWD");
        f += "/" + file;
        std::fstream fs;
        fs.open(f, std::fstream::in);
        std::stringstream ss;
        ss << fs.rdbuf();
        fs.close();
        std::string t;
        while(getline(ss, t, '\n')) {
            if(lastLines.size() == n) {
                lastLines.erase(lastLines.begin());
            }
            lastLines.push_back(t);
        }
    } else {
        std::string data = readLine();
        std::stringstream ss(data);
        std::string t;
        //std::vector<std::string> lastLines;
        while(getline(ss, t, '\n')) {
            if(lastLines.size() == n) {
                lastLines.erase(lastLines.begin());
            }
            lastLines.push_back(t);
        }
    }
    for(std::string item : lastLines) {
        print(item + '\n');
    }
}
