#pragma once

#include <stdlib.h>
#include <string>
#include <regex>
#include <iostream>
#include <exception>

std::string varParse(std::string str) {
    std::regex reg("\\$[A-Za-z0-9]+");
    std::smatch matches;
    while(std::regex_search(str, matches, reg)) {
        //std::cout << matches.str() << std::endl;
        std::string variable;
            variable = getenv(matches.str().substr(1).c_str());
        if(variable != "") {
            std::regex tempReg("\\" + matches.str());
            str = std::regex_replace(str, tempReg, variable);
        } else {
            std::regex tempReg("\\" + matches.str());
            str = std::regex_replace(str, tempReg, "");
        }
        //str = matches.suffix().str();
    }
    return str;
}
