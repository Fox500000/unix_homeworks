#pragma once

#include <string.h>
#include <string>
#include <unistd.h>

void print(std::string str) {
    write(STDOUT_FILENO, str.c_str(), str.length());
}

void print(char *str) {
    write(STDOUT_FILENO, str, strlen(str));
}

void print(const char *str) {
    write(STDOUT_FILENO, str, strlen(str));
}

std::string readLine() {
    char buffer[5000] = {0};
    int size = read(STDIN_FILENO, buffer, 5000);
    buffer[size - 1] = 0;
    std::string result = buffer;
    return result;
}
