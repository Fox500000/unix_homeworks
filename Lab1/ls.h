#pragma once

#include <iostream>
#include <sys/types.h>
#include <string>
#include <sys/stat.h>
#include <dirent.h>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "printstd.h"

std::string permissions(std::string file, bool l, bool i, bool a) {
    struct stat st;
    std::string mode = "";
    if(stat(file.c_str(), &st) == 0) {
        mode_t perms = st.st_mode;
        if(l) {
            mode += S_ISDIR(perms) ? 'd' : S_ISLNK(perms) ? 'l' : '-';
            mode += (perms & S_IRUSR) ? 'r' : '-';
            mode += (perms & S_IWUSR) ? 'w' : '-';
            mode += (perms & S_IXUSR) ? 'x' : '-';
            mode += (perms & S_IRGRP) ? 'r' : '-';
            mode += (perms & S_IWGRP) ? 'w' : '-';
            mode += (perms & S_IXGRP) ? 'x' : '-';
            mode += (perms & S_IROTH) ? 'r' : '-';
            mode += (perms & S_IWOTH) ? 'w' : '-';
            mode += (perms & S_IXOTH) ? 'x' : '-';
        }
        if(i) {
            mode += "\t";
            mode += std::to_string(st.st_ino);
        }
    }
    return mode;
}

void ls(std::string path, bool l, bool i, bool a) {
    DIR *directory;
    struct dirent *ent;
    if(path[0] == '~') path = getenv("HOME") + path.substr(1);
    if((directory = opendir(path.c_str())) != NULL) {
        while((ent = readdir(directory)) != NULL) {
            if((*ent->d_name == '.' && a) || *ent->d_name != '.') {
                //std::cout << std::setw(15) << permissions(path + "/" + ent->d_name, l, i, a) << " " << ent->d_name << std::endl;
                print(permissions(path + "/" + ent->d_name, l, i, a) + "\t" + ent->d_name + '\n');
            }
        }
    } else {
        perror("ls error");
    }
}
