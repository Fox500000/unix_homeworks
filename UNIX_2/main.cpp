#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

//struct dirent {
//               ino_t          d_ino;       /* Inode number */
//               off_t          d_off;       /* Not an offset; see below */
//               unsigned short d_reclen;    /* Length of this record */
//               unsigned char  d_type;      /* Type of file; not supported
//                                              by all filesystem types */
//               char           d_name[256]; /* Null-terminated filename */
//};

int main(int args, char **argv)
{
    DIR *directory;
    struct dirent *dp;
    struct stat statusBuffer;
    const char *rights = "rwxrwxrwx";
    int opt = 0;
    const char *optString = "l";
    bool display = false;

    opt = getopt(args, argv, optString);
    switch(opt) {
    case 'l':
        display = true;
    default: break;
    }
    directory = opendir("/");
    while((dp = readdir(directory))) {
        if(display) {
        lstat(dp->d_name, &statusBuffer);
        auto mode = statusBuffer.st_mode;
        S_ISDIR(mode) ? cout << "d" : cout << "-";
        mode &= 0777;
        auto right_ind = 0;
        while (right_ind < 9) {
            (mode & 256) ? cout << rights[right_ind] : cout << "-";
            mode <<= 1;
            ++right_ind;
        }
        }
        cout << "   " << dp->d_name << endl;
    }
    closedir(directory);
    return 0;
}
