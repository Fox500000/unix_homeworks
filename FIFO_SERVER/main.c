#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#define FIFO_FSERVER "/home/xfox/Projects/FIFO_SERVER/FIFO_FILE"
int main()
{
    unlink(FIFO_FSERVER); // remove FIFO file, if exist
    int fp;
    int n;
    const int bufsize = 512;
    char readbuf[512] = {'\0'};

    umask(0); // set file mask for creating files
    int state = mknod(FIFO_FSERVER, S_IFIFO | S_IRWXU | S_IRWXO | S_IRWXG, 0); // create FIFO file
    while(1)
    {
        // read from client
        fp = open(FIFO_FSERVER, O_RDONLY); // open FIFO file
        FILE* file = fdopen(fp, "r");
        while(fgets(readbuf, bufsize, file) != NULL) // read while data exists
        {
            readbuf[strcspn(readbuf, "\n")] = 0;
            printf("FIFO connected...\nData:\n%s\n", readbuf); // show received data
            pid_t pid = fork();
            char string[100000] = {'\0'};
            time_t start;
            int fp2 = 0;
            switch(pid) {
            case 0:
                //Child
                fp2 = open(readbuf, O_WRONLY);
                start = time(0);
                while(difftime(time(0), start) <= 30) {
                    pid_t current = getpid();
                    printf("CHILD PID = %d : %f\n", current, difftime(time(0), start));
                    setpriority(PRIO_PROCESS, current, rand()%11);
                    //sleep(1);
                    write(fp2, (char)(rand() % 256), 1);
                    //strcat(string, "A\0");
                }
                close(fp2);
                return 0;
            case -1:
                return -1;
            default:
                //Parent
                continue;
            }
        }
        close(fp);
        // write echo replay to client

        printf("Press\n");
        //getchar();
    }
    return 0;
}
