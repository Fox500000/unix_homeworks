#include <iostream>
#include <cmath>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>

using namespace std;

double Pi() {
    double result = 0;
    for(int i = 0; i < 100000; i++) {
        result += pow(-1, i) / (2 * i + 1);
    }
    return result * 4;
}

double ex(double x) {
    double result = 1.0;
    double p = x;
    for(int i = 2; i < 1000000; i++) {
        result += p;
        p = (p * x) / i;
    }
    return result;
}

void function1(int sig) {
    //signal(SIGUSR1, function1);
}

int main() {
    double x = 0.0;
    double mu = 0.0;
    double disp = 0.4472;
    int pipe1[2];
    int pipe2[2];
    pipe(pipe1);
    pipe(pipe2);
    pid_t proc1 = fork();
//    int fd = 0;
    double result;
    switch(proc1) {
    case 0:
        //Child process
        signal(SIGUSR1, function1);
        pause();
        //dup2(pipe1[1], STDOUT_FILENO);
        if(execl("UNIX_4_1", (char*)NULL)) throw exception();
        result = Pi();
        write(pipe1[1], &result, sizeof(result));
        exit(EXIT_SUCCESS);
    case -1:
        cout << "Didn't create child processes" << endl;
        break;
    default:
        //Parent process
        break;
    }
    pid_t proc2 = fork();
    switch(proc2) {
    case 0:
        //Child process
        signal(SIGUSR1, function1);
        pause();
        result = ex(-pow(x - mu, 2) / (2 * pow(disp, 2)));
        write(pipe2[1], &result, sizeof(result));
        exit(EXIT_SUCCESS);
    case -1:
        cout << "Didn't create child processes" << endl;
        break;
    default:
        //Parent process
        break;
    }
    sleep(1);
    int status1, status2;
    kill(proc1, SIGUSR1);
    kill(proc2, SIGUSR1);
    waitpid(proc1, &status1, 0);
    waitpid(proc2, &status2, 0);
    if(WIFEXITED(status1) && WIFEXITED(status2)) {
        double output1 = .0;
        double output2 = .0;
        read(pipe1[0], &output1, sizeof(output1));
        read(pipe2[0], &output2, sizeof(output2));
        cout << output1 << " | " << output2 << endl;
        result = (1 / (disp * sqrt(2 * output1))) * output2;
        cout << result << endl;
    }
    return 0;
}
