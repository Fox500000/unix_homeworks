#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

int main()
{
    string serverFifo = "~/Projects/UNIX_6/serverFifo";
    umask(0);
    mknod(serverFifo.c_str(), S_IFIFO | 0777, 0);
    return 0;
}
