#include <iostream>
#include <ucontext.h>
using namespace std;

static ucontext_t uctx_main, uctx_func1, uctx_func2;

static void func1(void) {
    cout << "Func1 EEEEE" << endl;
    cout << "Swapping to func2..." << endl;
    swapcontext(&uctx_func1, &uctx_func2);
    cout << "ROCK AND STONE" << endl;
}

static void func2(void) {
    cout << "Func2 EEEEE" << endl;
    cout << "Swapping to func1..." << endl;
    swapcontext(&uctx_func2, &uctx_func1);
    cout << "STONE AND ROCK" << endl;
}
//NANI THE F
static void func_main(void (*f)(void)) {
    char stack1[4096], stack2[4096];
    getcontext(&uctx_func1);
    uctx_func1.uc_link = &uctx_main;
    uctx_func1.uc_stack.ss_sp = stack1;
    uctx_func1.uc_stack.ss_size = sizeof(stack1);
    makecontext(&uctx_func1, f, 0);

    getcontext(&uctx_func2);
    uctx_func2.uc_link = &uctx_func1;
    uctx_func2.uc_stack.ss_sp = stack2;
    uctx_func2.uc_stack.ss_size = sizeof(stack2);
    makecontext(&uctx_func2, func2, 0);

    cout << "SWAPPING" << endl;
    swapcontext(&uctx_main, &uctx_func1);
}

int main()
{
    func_main(func1);
    return 0;
}
