#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <iostream>
#include <cmath>
#include <unistd.h>

static int ID = 456;
static std::string fileKey = "../SHAREDMEM";

struct Data {
    int val = 0;
    int simple = -1;
    bool gottem = false;
};

bool check(int number) {
    int sqrtNum = sqrt(number);
    for(int i = 2; i <= sqrtNum; i++) {
        if (number % i == 0) return false;
    }
    return true;
}
int main() {
    Data *storage;
    key_t key = 456;
    //if(key = ftok(fileKey.c_str(), ID) == -1) std::cout << "Stinky";
    int shmId = shmget(key, sizeof(Data), 0666);
    storage = (Data*)shmat(shmId, (void*)0, 0);
    std::cout << "Waiting for msg" << std::endl;
    while(storage->gottem == false) usleep(200);
    storage->simple = check(storage->val);
    return 0;
}
