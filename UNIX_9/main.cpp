#include <iostream>
#include <sys/shm.h>
#include <sys/types.h>
#include <string>
#include <unistd.h>

struct Data {
    int val = 0;
    int simple = -1;
    bool gottem = false;
};

static std::string fileKey = "SHAREDMEM";
static int ID = 456;

int main() {
    Data *storage;
    system("ls");
    key_t key;
    //if(key = ftok(fileKey.c_str(), ID) == -1) std::cout << "No";
    key = 456;
    perror("");
    int shmId = shmget(key, sizeof(Data), IPC_CREAT | 0666);
    storage = (Data*)shmat(shmId, (void*)0, 0);
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;
    storage->val = number;
    storage->simple = -1;
    storage->gottem = true;
    std::cout << "Waiting for reply..." << std::endl;
    while(storage->simple == -1) {
        usleep(200);
    }
    std::cout << "Value is " << (storage->simple > 0 ? "simple" : "not simple") << std::endl;
    shmdt(storage);
	return 0;
}
