#include <iostream>
#include <cmath>
#include <pthread.h>

using namespace std;

double Pi() {
    double result = 0;
    for(int i = 0; i < 100000; i++) {
        result += pow(-1, i) / (2 * i + 1);
    }
    return result * 4;
}

double ex(double x) {
    double result = 1.0;
    double p = x;
    for(int i = 2; i < 1000000; i++) {
        result += p;
        p = (p * x) / i;
    }
    return result;
}

void* thread_pi(void* attr) {
    double* result = new double;
    *result = Pi();
    return result;
}

void* thread_ex(void* attr) {
    double* result = new double;
    double* input = (double*)attr;
    *result = ex(*input);
    return result;
}

int main()
{
    pthread_t thread1, thread2;
    double x = 0.0;
    double mu = 0.0;
    double disp = 0.4472;
    double temp_x = -pow(x - mu, 2) / (2 * pow(disp, 2));
    pthread_create(&thread2, NULL, thread_ex, (void*)&(temp_x));
    pthread_create(&thread1, NULL, thread_pi, NULL);
    double *res1, *res2;
    pthread_join(thread1, (void**)&res1);
    pthread_join(thread2, (void**)&res2);
    double result = (1 / (disp * sqrt(2 * *res1))) * *res2;
    cout << result << endl;
    return 0;
}
