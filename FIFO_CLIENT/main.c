#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

#define FIFO_FSERVER "/home/xfox/Projects/FIFO_SERVER/FIFO_FILE"
#define FIFO_FCLIENT "FIFO_FILE"
int main()
{
    printf("KEK\n");
    //fflush(stdout);
    int fp;
    int bufsize = 4096;
    char readbuf[bufsize];
    umask(0);
    unlink(FIFO_FCLIENT);
    mknod(FIFO_FCLIENT, S_IFIFO | 0777, 0);
    char* full_path = realpath(FIFO_FCLIENT, NULL);
    fp = open(FIFO_FSERVER, O_WRONLY);
    if(fp == 0)
    {
        perror("open error");
        exit(1);
    }
    printf("Sending fresh fuel cells for the drop pod...\n");
    // send data to server
    char* val = strcat(full_path, "\n");
    write(fp, val, strlen(full_path));
    close(fp);

    // read echo replay from server
    if((fp = open(FIFO_FCLIENT, O_RDONLY | O_NONBLOCK)) < 0)
    {
        perror("open error");
        exit(1);
    }
    sleep(2);
    char temp[1];
    unsigned long counter = 0;
    //sleep(33);
    while(read(fp, temp, 1) > 0) {
        printf("%s\n", temp);
        ++counter;
    }
    perror("Asd");
    printf("Servers echo reply: %ld \n", counter);
    close(fp);
    return 0;
}
