#include <iostream>
#include <sys/msg.h>
#include <sys/types.h>
#include <string>
#include <unistd.h>
const int msgKey = 456;

struct msgData {
    long type;
    char text[256] = {'\0'};
} static msg;

int main() {
    int msgId = msgget(msgKey, IPC_CREAT | 0666);
    int number;
    int n;
    std::cout << "Enter amount of msg: ";
    std::cin >> n;
    for(int i = 0; i < n; i++) {
        std::cout << "Enter number: ";
        std::cin >> number;
        msg.type = 1;
        int pos = 0;
        for(char item : std::to_string(number)) {
            msg.text[pos] = item;
            ++pos;
        }
        msgsnd(msgId, &msg, 256, 0);
        for(int i = 0; i < 256; i++) {
            msg.text[i] = '\0';
        }
    }
    //sleep(3);
    std::cout << "Waiting for reply..." << std::endl;
    for(int i = 0; i < n; i++) {
        msgrcv(msgId, &msg, 256, 2, 0);
        std::string data(msg.text);
        std::cout << "Reply: " << data << std::endl;
    }
    msgctl(msgId, IPC_RMID, nullptr);
	return 0;
}
