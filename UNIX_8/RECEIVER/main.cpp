#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <iostream>
#include <cmath>

static int msgKey = 456;

struct msgData {
    long type = 1;
    char text[256] = {'\0'};
} static msg, msg2;

std::string check(int number) {
    int sqrtNum = sqrt(number);
    for(int i = 2; i <= sqrtNum; i++) {
        if (number % i == 0) return std::to_string(number) + " - not simple";
    }
    return std::to_string(number) + " - simple";
}
int main() {
    int msgId = msgget(msgKey, IPC_CREAT | 0666);
    std::cout << "Waiting for msg" << std::endl;
    while(true) {
        errno = 0;
        msgrcv(msgId, &msg, 256, 1, 0);
        if(errno == EIDRM) break;
        std::string data(msg.text);
        std::cout << "Data: " << data << std::endl;
        int number = std::stoi(data);
        std::string result = check(number);
        std::cout << "Sending result: " << result << std::endl;
        msg2.type = 2;
        int pos = 0;
        for(char item : result) {
            msg2.text[pos] = item;
            ++pos;
        }
        msgsnd(msgId, &msg2, 256, 0);
        for(int i = 0; i < 256; i++) {
            msg2.text[i] = '\0';
        }
    }
    return 0;
}
