#include <iostream>
#include <unistd.h>
#include <fcntl.h>
using namespace std;

int main()
{
    const int size = 100;
    char buffer[size] = {'\0'};
    int fd = creat("Out.txt", 0666);
    size_t len = 2;
    while(len > 1) {
    len = read(0, buffer, size);
    write(fd, buffer, len);
    }
    close(fd);

    fd = open("Out.txt", O_RDONLY);
    len = read(fd, buffer, size);

    for(size_t i = 0; i < len; i++) {
        cout << buffer[i];
    }
    cout << endl;
    close(fd);
    return 0;
}
